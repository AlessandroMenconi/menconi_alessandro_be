import { Component, Inject, OnInit } from '@angular/core';
import { ApiGameService } from '../services/api-game.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss'],


})
export class ListaComponent implements OnInit {

  POST: any;
  page: number = 1;
  count: number = 100;
  listSize: number = 10;
  listSizes:any=[10,15,20];
  optionSelect:any=['Male','Female']
  closeResult!: string;
  vettoreDetail: any[];
  searchName:any;
  searchGender:any;

  constructor(public tService: ApiGameService, private modalService: NgbModal) {
    this.vettoreDetail = [];
  };

  ngOnInit(): void {
    this.listCharacters();
  }

  listCharacters(): void {
    this.tService.getListaPersonaggi(this.page,this.listSize).subscribe(res => {
      this.POST = res;
    });
  }

  filteredListCharacters(): void {
    this.tService.getFilteredPersonaggi(this.searchName).subscribe(res => {
      this.POST = res;
    });
  }

  changeList(event: any): void {
    this.page = event;
    this.listCharacters();
  }
  changeSizeList(event:any):void{
    this.listSize=event.target.value;
    this.page=1;
    console.log(this.listSize)
    this.listCharacters()
  }


  showDetail(event: any, post: any, content: any) {
    event.preventDefault();
    this.vettoreDetail.push(post);


    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result: any) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason: any) => {
      this.vettoreDetail.shift();
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';

    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';

    } else {
      return `with: ${reason}`;

    }
  }

  doSearch() {
    this.filteredListCharacters();
  }
}
