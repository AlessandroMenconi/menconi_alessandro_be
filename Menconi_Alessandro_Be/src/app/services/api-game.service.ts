import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiGameService {

  constructor(public http: HttpClient) {
  }


  getListaPersonaggi(page: number, listSize: number): Observable<any> {
    return this.http.get('https://anapioficeandfire.com/api/characters?page=' + page + '&pageSize=' + listSize + '');
  };

  getFilteredPersonaggi(filterName: string): Observable<any> {
    let filters = '';
    if (filterName) {
      filters += 'name=' + filterName;
    }

    return this.http.get('https://anapioficeandfire.com/api/characters?' + filters);
  }
}

